---
title: "in class oct/4/2108 assigment"
author: "Matt H"
date: "10/4/2018"
output: html_document
---
packages required
```{r}
library(data.table)
library(dplyr)
library(benchmarkme)
library(lubridate)
library(ggplot2)
library(Hmisc)
library(markdown)
library(profvis)

```

The first question deal with reading int he first 100 rows of the file
```{r flights problem 1}
x <- read.csv(file="flights_time.csv",nrows=100)
```
The second Question deals with using read.csv,read_csv and fread
```{r}
profvis({
  a <- read.csv(file="flights_time.csv")
  b <- read_csv(file="flights_time.csv")
  c <- fread(file="flights_time.csv")
})
```
The microbechmark code indicates that the "fread" function worked the fastest.

The Next Question 3, which is to remove two intances or all 3, could use rm function

```{r}
rm(a)
rm(b)
rm(c)
```
From question 2, pick the best function to read file and use mircobenchmark to get average ime across 10 iterations
```{r}
ques_4 <- microbenchmark(
  c <- fread(file="flights_time.csv"),
  times = 10
)
ques_4

```
Question 5, Calcuulte the number of flights by month 
```{r}
a <- read.csv(file="flights_time.csv")

month_df = tbl_df(a)

ques_5 = month_df %>%
  group_by(month)%>%
  summarise(n=n())
```
Qestion 6
```{r}
a$date <- as.Date(with(a, paste(year, month, day,sep="-")), "%Y-%m-%d")

a$real_weekday <- wday(a$date) #the week day

day_week_df = tbl_df(a)

ques_6 <- day_week_df %>%
  dplyr::group_by(real_weekday) %>%
  dplyr::summarise(n=n())

group_6
```
Question  7 : group by day of the week for flights from JFK to SFO
```{r}
a$date <- as.Date(with(a, paste(year, month, day,sep="-")), "%Y-%m-%d")

a$real_weekday <- wday(a$date) #the week day

SFO = filter(a, dest == 'SFO' & origin == 'JFK')

Ques_7 = SFO %>%
  group_by(real_weekday) %>%
  summarise(n=n())
```
Question 8: create bar plot of top 10 routes by frequency of flights
```{r}
#find top 10 routes
top_10 <- a %>%
  group_by(dest) %>%
  summarise(n=n())

top_10 %>%
    arrange((n))

top_10 <- slice(top_10,95:104)
View(top_10)

ques_8 <- ggplot(top_10,aes(x= n , y = dest))+geom_bar()
ques_8


```

Question 10
```{r}



```
